document.addEventListener("DOMContentLoaded", init);

function init() {
  let query = window.matchMedia("(min-width: 1081px)");

  if (query.matches) {
    //if the page is wider than 1080px
    document.querySelector('data-turbolinks="false').style.display = "none";
  } else {
    //if the page is narrower than 1081px
    document.querySelector('data-turbolinks="false').style.display = "block";
  }
}
